import * as Tools from './Tools.js';

let xmlns = "http://www.w3.org/2000/svg",
    // getSel = Tools.getSel,
    gid = Tools.gid;

function svgInit(name, arg) {
    let
        svgCanvas = gid(`svg-${name}`),
        path = gid(`${name}_svg-path`);
    path.setAttribute('stroke-width', arg.strokeWidth);
    svgCanvas.setAttribute('width', arg.width + arg.strokeWidth);
    svgCanvas.setAttribute('height', arg.height + arg.strokeWidth);
    svgCanvas.setAttribute('viewBox', `${0-arg.strokeWidth/2} ${0-arg.strokeWidth/2}
                ${arg.width + arg.strokeWidth} ${arg.height + arg.strokeWidth}`)

}

function setSvgElem(shape, el) {
    for (let prop in el) {
        if (el.hasOwnProperty(prop)) {
            // console.log(el)
            if (prop==='xlink:href') {
                shape.setAttributeNS('xlink:href', prop, el[prop])
            }else if (prop==='id'){
                shape.setAttributeNS('', 'id',`${el.name}_${el[prop]}`)

            }else{
                shape.setAttributeNS('', prop, el[prop])
            }
        }
    }
}

function setPropSvg(svgEl, attr) {
    let SvgEl = Tools.gid(svgEl);
    for (let prop in attr) {
        if (attr.hasOwnProperty(prop)) {
            // console.log(attr)
            if (prop==='xlink:href') {
                SvgEl.setAttributeNS('xlink:href', prop, attr[prop])
            }else{
                SvgEl.setAttributeNS('', prop, attr[prop])
            }
        }
    }
}

function renderSvgElem(svg, el) {

    let shape = Tools.gid(`${el.name}_${el.id}`);
    if (!shape) {
        // console.log(`${el.name}${el.id}`)
        // console.log(shape)
        // console.log(svg)
        shape = document.createElementNS(xmlns, el.shape);
        svg.appendChild(shape);
    }
    setSvgElem(shape, el)
}

let timeID = new Map();

function runAnimate(el, event, name, totalDur, ...animations ) {
    animations.forEach(function (item) {
        let anim = gid(`${name}_${item}`);
        // console.log(`${name}${item}`)
        // console.log(anim)
        // console.log(anim.tagName)
        if (anim && anim.tagName==='animate') {
            el.addEventListener(event, () => {
                // console.log('run total dur')
                anim.beginElement();
                if (totalDur){
                    // console.log(timeID.get(item))
                    clearTimeout(timeID.get(item));
                    // timeID.delete(item)
                    timeID.set(item, setTimeout(()=>{
                        // console.log('end total dur')
                        anim.endElement()
                    }, totalDur))
                }
            })
        }
    })
}

function setAnimate(el, event, idAnim, attr) {
    let anim = gid(idAnim);
    if (anim && anim.tagName==='animate') {
        el.addEventListener(event, () => {
            for (let prop in attr) {
                if (attr.hasOwnProperty(prop)) {
                    // console.log(attr)
                    if (prop === 'xlink:href') {
                        anim.setAttributeNS('xlink:href', prop, attr[prop])
                    } else {
                        anim.setAttributeNS('', prop, attr[prop])
                    }
                }
            }
        })
    }
}


function stopAnimate(el, event, totalDur, name, ...animations) {
    animations.forEach(function (item) {
        // console.log(item)
        let anim = gid(`${name}_${item}`);
        // console.log(`${name}${item}`)
        if (anim && anim.tagName==='animate') {
            el.addEventListener(event, () => {
                // console.log('end anim');
                if (totalDur){
                    setTimeout(()=>{
                        // console.log('end total dur');
                        anim.endElement()
                    }, totalDur)
                }
                // anim.endElement()
            })
        }
    })
}

//    function TotalLength(){
//        let path = gid('case');
////        var path = document.querySelector('#case');
//        if (path) {
//            var len = Math.round(path.getTotalLength());
//            console.log("Длина пути - " + len);
//        }
//    };
//    TotalLength()


export { runAnimate, setPropSvg, setAnimate, renderSvgElem, svgInit, stopAnimate, setSvgElem }

