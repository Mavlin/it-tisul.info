<style>
    #svg-phone{
        /*outline: 1px solid blue;*/
        /*fill: blue;*/
        height: 100%;
        width: 60%;
        /*width: 100px;*/
        /*height: 150px;*/
    }

</style>

<template id="tpl-phone">
    <div class="position-center">
        <svg xmlns="http://www.w3.org/2000/svg" id="svg-phone"
             class="svg-canvas"
             width="262" height="416"
             viewBox="-3 -3 262 416"
        >
            <!--width="256", height="410", real size-->

            <circle id="glob-phone" class="glob"
                    mask="url(#phone_mask)"
                    r=0 cx=128 cy=180
            >
                <animate
                        id="phone_anim"
                        attributeName="r"
                        values="0; 170"
                        dur=".15s"
                        begin="indefinite"
                        repeatCount="1"  fill="freeze"  calcMode="linear">
                </animate>
                <animate
                        id="phone_anim2"
                        attributeName="r"
                        values="170;0"
                        dur=".15s"
                        begin="indefinite"
                        repeatCount="1"  fill="freeze"  calcMode="linear">
                </animate>
            </circle>

            <g
                    class="svg-path"
                    transform="translate(0,-149)"
            >
                <path id="phone_svg-path" stroke-opacity="0"
                      d="M38 149
                      h180
                      c21 0 38 18 38 39
                      v333
                      c0 21-17 38-38 38
                      H38
                      c-21 0-38-17-38-38
                      V188
                      c0-21 17-39 38-39z
                      m141 20H77v12h102z
                      m-51 365a26 26 0 1 0 0-51 26 26 0 0 0 0 51z
                      M26 457h205V201H26z"
                >
                    <animate
                            id="phone_anim3"
                            attributeName="stroke-opacity"
                            values="0;1"
                            dur=".15s"
                            begin="indefinite"
                            repeatCount="1"  fill="freeze"  calcMode="linear">
                    </animate>
                    <animate
                            id="phone_anim4"
                            attributeName="fill-opacity"
                            values="1;0"
                            dur=".15s"
                            begin="indefinite"
                            repeatCount="1"  fill="freeze"  calcMode="linear">
                    </animate>
                    <animate
                            id="phone_anim5"
                            attributeName="stroke-opacity"
                            values="1;0"
                            dur=".15s"
                            begin="indefinite"
                            repeatCount="1"  fill="freeze"  calcMode="linear">
                    </animate>
                    <animate
                            id="phone_anim6"
                            attributeName="fill-opacity"
                            values="0;1"
                            dur=".15s"
                            begin="indefinite"
                            repeatCount="1"  fill="freeze"  calcMode="linear">
                    </animate>
                </path>
                <!--смещение маски учитывается в маскируемом объекте
                поэтому лучше назначать абсолютные координаты целевому объекту и смещение маске
                -->
                <defs>
                    <mask id="phone_mask">
                        <path
                                transform="translate(0,-149)"
                                d="M26 457h205V201H26z"
                                fill="white"
                        ></path>
                    </mask>
                </defs>
            </g>

        </svg>
    </div>

    <script type="module">
//        import * as Tools from '../js/Tools.js';
//        import * as Svg from '../js/Svg.js';
        (function () {
            const
                name='phone',
                gadget= {
                    strokeWidth: 6,
                    width: 256,
                    height: 410
                },
                parent = Tools.getSel(`#tpl-${name}`).parentNode.parentNode,
//                trigger = parent
                trigger = parent.querySelector('.caption')
            Svg.svgInit(name, gadget)
            Svg.runAnimate(trigger, 'mouseenter', name, false, 'anim','anim3','anim4')
            Svg.runAnimate(trigger, 'mouseleave', name, false, 'anim2','anim5','anim6')
        })()

    </script>
</template>


