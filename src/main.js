'use strict';
import "./assets/style.scss";
import * as Tools from './js/Tools.js';
import * as Svg from './js/Svg.js';
// import './vendors/foundation-6.5.1/js/vendor/foundation'
// import 'script-loader!foundation/foundation.min'

// import 'foundation/foundation.min'
// import './vendors/foundation-6.5.1/js/app'
// require('foundation-sites/dist/foundation.js');

let my$ = Tools.getSel,
    $$ = Tools.getSels,
    gid = Tools.gid,
    dialogBoxes = $$('.dialog'),
    content = my$('body'),
    overlay = my$('.overlay'),
    case_fone = my$('.content-wrap'),
    actions = {
        openPopUp: openPopUp,
        closePopUp: closePopUp,
        sendMessage: sendMessage,
    },
    openPopUps = new Map();

content.addEventListener('submit',(e)=> {
    // debugger
    e.preventDefault()
    e.stopPropagation()
    // return
})

content.addEventListener('click',(e)=>{
    e.stopPropagation()
    // console.log(e.target)
    // debugger
    if (e.target && e.target.type){
        let type = e.target.type;
        switch (type){
            case 'submit':
                e.preventDefault();
                console.log('preventDefault')
        }
    }
    e.stopPropagation();
    let target, act, action, el;
    if (e.target) {
        // debugger
        target = e.target;// || e.target.closest('[data-action]')
        // dataset = e.target.dataset || e.target.closest('[data-action]')
        if (target){
            el = target.closest('[data-action]');
            if (el) {
                action = el.dataset.action;
                if (action) {
                    act = action.split(':');
                    if (act) {
                        actions[act[0]](act[1] === 'this' ? e : act[1])
                    }
                }
            }
        }
    }
});

function sendMessage(arg) {
    // console.log('sendMessage')
    // console.log(arg)
    // debugger
    let el = arg.target.closest('form'),
        form = new Tools.Form( el ),
        reqData = form.getData(),
        data = new FormData(), i;
    for (i in reqData) {
        if (reqData.hasOwnProperty(i)) data.append(i, reqData[i]);
    }
    let req = {
        'url': './init.php',
        'body': data,
        'method': 'post'
    };
    new Tools.Fetch(req).run()
        .then(
            response => {
                console.log( response );
                if (response.status === 'ok'){
                    let submitButton = form.submitButt;
                    submitButton.innerHTML = 'доставлено';
                    submitButton.setAttribute('disabled', 'disabled');
                    form.form.addEventListener('input',()=>{
                        submitButton.removeAttribute('disabled');
                        submitButton.innerHTML = 'Отправить<i>&#xe79d;</i>'
                    }, {once:true})
                }
            }
        ).catch(
        error => console.log( error )
    )

}

let foneGrid = {
    row: ()=> {
        return Math.round(case_fone.scrollHeight/150)
    },
    column: ()=>{
        return Math.round(window.innerWidth/180)
    }
};

// window.addEventListener('resize', function(){setFone()});

window.addEventListener('load', function(){setFone()});

function setFone(width=1200) {
    // console.log(case_fone.scrollWidth)
    // debugger
    let fone = my$('.fone'), row, col, i = 0, k, ti = 0, column, fragment, icon;
    if (fone) {
        fone.innerHTML = '';
        if (case_fone.scrollWidth > width) {
            fone.style.height = case_fone.scrollHeight + 'px';
            row = foneGrid.row();
            col = foneGrid.column();
            while (i < row) {
                fragment = document.createDocumentFragment();
                column = document.createElement('div');
                column.classList.add('fone-column');
                k = 0;
                while (k < col) {
                    // column.insertAdjacentHTML('beforeEnd', "<i id='fc-icon-'" + ti++ + "></i>");
                    column.insertAdjacentHTML('beforeEnd', `<i id="fc-icon-${ti++}"></i>`);
                    k++
                }
                fragment.appendChild(column);
                fone.appendChild(fragment);
                i++;
            }
            setIcon()
        }
    }
}

let arr = ['&#xe800;','&#xe801;','&#xe802;','&#xe803;',
    '&#xe804;','&#xe805;','&#xe806;','&#xe807;','&#xe808;',
    '&#xe809;','&#xe80a;','&#xe80b;','&#xe80c;','&#xe80d;',
    '&#xe80e;','&#xe80f;','&#xf087;','&#xf108;','&#xf109;',
    '&#xf10a;','&#xf10b;'];

function setIcon() {
    let i=0, el, rnd, len, row, col;
    len = arr.length;
    row = foneGrid.row();
    col = foneGrid.column();
    while (i<row*col){
        el = gid('fc-icon-' + i++);
        // el = gid(`fc-icon-${i++}`);
        if (el){
            rnd = Tools.randomInteger(1, len)-1;
            el.innerHTML = arr[rnd]
        }
    }
}

if (overlay) {
    overlay.addEventListener('click', (e) => {
        // console.log('close overlay')
        closeAllPopUp();
        e.stopPropagation()
    });
}
// Close the dialog - press escape key // key#27
document.addEventListener('keyup', function(e) {
    if (e.keyCode === 27) {
        let cntOpenPopUps = openPopUps.size, i = 0;
        openPopUps.forEach((val, key, set)=>{
            if (++i === cntOpenPopUps){
                closePopUp(key);
                set.delete(key)
            }
        })
    }
});

// Close the dialog - click outside
function openPopUp(el) {
    // console.log(el)
    let tpl = gid('tpl-'+el), elId = gid(el);
    if (tpl) {
        elId.innerHTML = tpl.innerHTML
    }
    elId.style.display = 'block';
    setTimeout(() => {
        elId.classList.add('active');
        // debugger
        overlay.classList.add('active');
        // my$('.case').classList.add('active');
        openPopUps.set(el, 1); // list of opened popup windows
    }, 20);
    setTimeout(() => {
        let form = elId.querySelector('form'), field;
        if (form) {
            // debugger
            // form.reset()
            field = form.querySelector('[autofocus="autofocus"]');
            if (field) {
                field.focus()
            }
        }
    }, 200)
}


function closePopUp(e) {
    let dialog;
    // console.log(e)
    // debugger
    if (e.target){
        dialog = e.target.closest('.dialog')
    } else {
        dialog = gid(e)
    }
    dialog.classList.remove('active');
    openPopUps.delete(dialog.getAttribute('id'));
    // console.log(openPopUps)
    setTimeout(function () {
        dialog.style.display='none'
    },500);
    if (!$$('.dialog.active').length){
        overlay.classList.remove('active')
    }
}

function closeAllPopUp() {
    overlay.classList.remove('active');
    // debugger
    openPopUps.clear();
    for (let item in dialogBoxes){
        if (dialogBoxes.hasOwnProperty(item)) {
            // console.log(dialogBoxes[item])
            dialogBoxes[item].classList.remove('active');
            setTimeout(function () {
                dialogBoxes[item].style.display='none'
            },500)
        }
    }
}

window.Tools = Tools;
window.Svg = Svg;
//
Tools.renderTpl('logo_vector', '#name_logo');
Tools.renderTpl('name_vector', '#name_logo');
// Tools.renderTpl('pc', '#dev');

['pc','phone','tv','printer'].some((item)=>{
    // console.log(777)
    Tools.renderTpl(item)
});

// console.log($('body'))
// $(document).foundation()
