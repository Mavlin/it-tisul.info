<?php

namespace Controllers;

//include $_SERVER['DOCUMENT_ROOT']."./ApMailer/Mailer.php";
include $_SERVER['DOCUMENT_ROOT']."/ApMailer/Mailer.php";

class Sender extends Controller //
{
    public function __construct( $req ){
        if ($req !== null) {
            parent::__construct( $req );
        }
    }

    public function sendMail(){
        $this->decodeRequest($this->request[(string)__FUNCTION__]['data']);
        $body = $this->getRequestParam( $this->request, 'body');
        $message = false;
        if (!empty($body)){ //

            $config = [
                'defaultFrom' => 'tisul-electro@mail.ru',
//                'onError'     => function($error, $message, $transport) { echo $error; },
//                'afterSend'   => function($text, $message, $layer) { echo $text; },
                'transports'  => [
                    // Сохранение всех писем в папке
//                    ['file', 'dir'  => __DIR__ .'/mails'],
                    // Отправка писем через mail.ru, используя SSL и авторизацию
//                    ['smtp', 'host' => 'smtp.yandex.ru', 'ssl' => true, 'port' => '465',
//                        'login' => 'mavlin.d', 'password' => 'xKE9reeHSdpN'],
                    ['smtp', 'host' => 'smtp.mail.ru', 'ssl' => true, 'port' => '465',
                        'login' => 'tisul-electro@mail.ru', 'password' => 'R75l5coIXpib'],
                ],
            ];
            Mailer()->init($config);
            $message = Mailer()->newHtmlMessage();
            $message->setContent($body)
                ->setSubject('Вопрос или просьба перезвонить')
                ->addRecipient(['mavlind@list.ru']);
        }
        if (Mailer()->sendMessage($message)) {
            $responseCode = $this->success;
            $this->setResponse('data', true);
        } else {
            $responseCode = $this->SomethingWrong;
        }
        $this->setResponse('message', $responseCode['message']);
        $this->setResponse('status', 'ok');
        $this->setResponse('code', $responseCode['code']);
    }

}
