let tpl = `<template id="tpl-gadget">
    <style>
        #svg-gadget{
            /*outline: 1px solid blue;*/
            /*fill: blue;*/
            height: 100%;
            width: 70%;
            /*width: 100px;*/
            /*height: 150px;*/
        }

    </style>

    <div class="position-center">
        <!-- ширину и высоту svg и viewBox лучше продублировать здесь, тк сначала браузер отрисует
        так как указано здесь, а потом исполниться скрипт, те будет мигание -->
        <svg xmlns="http://www.w3.org/2000/svg" id="svg-gadget"
             class="svg-canvas"
             width="712"
             height="1032"
             viewBox="-4 -4 712 1032"
        >
            <!--width="704", height="1024", real size-->
            <circle id="glob-gadget" class="glob" r=0 cx=361 cy=512
                    mask="url(#gadget_mask)">
                <animate
                        id="gadget_anim"
                        attributeName="r"
                        values="0; 450"
                        dur=".15s"
                        begin="indefinite"
                        repeatCount="1"  fill="freeze"  calcMode="linear">
                </animate>
                <animate
                        id="gadget_anim2"
                        attributeName="r"
                        values="450;0"
                        dur=".15s"
                        begin="indefinite"
                        repeatCount="1"  fill="freeze"  calcMode="linear">
                </animate>
            </circle>

            <g
                    class="svg-path"
                    transform="translate(-160,64)"
                    stroke="none"
            >

                <path id="gadget_svg-path" stroke-opacity="0"
                      d="M768 960h-512 c-53 0-96-42-96-96 v-832c0-53.056
         42.976-96 96-96h512c52.992 0 96 43.008 96 96v832c0 53.056-43.008 96-96 96z
         M800 32c0-17.632-14.368-32-32-32h-512c-17.664 0-32 14.304-32 32v64.128h576v-64.128z
          M800 128.128h-576v639.872h576v-639.872z
           M800 800h-576v64c0 17.664 14.336 32 32 32h512c17.632
            0 32-14.336 32-32v-64z
            M576 848c0-8.832-7.2-16-16-16h-96c-8.864 0-16 7.168-16 16v0c0
             8.864 7.136 16 16 16h96c8.8 0 16-7.136 16-16v0z
             M544 48.128c0-8.8-7.2-16-16-16
             h-32c-8.832 0-16 7.2-16 16v0c0 8.864 7.168 16 16 16h32c8.8 0 16-7.136 16-16v0z"
                >
                    <animate
                            id="gadget_anim3"
                            attributeName="stroke-opacity"
                            values="0;1"
                            dur=".15s"
                            begin="indefinite"
                            repeatCount="1"  fill="freeze"  calcMode="linear">
                    </animate>
                    <animate
                            id="gadget_anim4"
                            attributeName="fill-opacity"
                            values="1;0"
                            dur=".15s"
                            begin="indefinite"
                            repeatCount="1"  fill="freeze"  calcMode="linear">
                    </animate>
                    <animate
                            id="gadget_anim5"
                            attributeName="stroke-opacity"
                            values="1;0"
                            dur=".15s"
                            begin="indefinite"
                            repeatCount="1"  fill="freeze"  calcMode="linear">
                    </animate>
                    <animate
                            id="gadget_anim6"
                            attributeName="fill-opacity"
                            values="0;1"
                            dur=".15s"
                            begin="indefinite"
                            repeatCount="1"  fill="freeze"  calcMode="linear">
                    </animate>
                </path>
                <defs>
                    <mask id="gadget_mask">
                        <path></path>
                    </mask>
                </defs>
            </g>
        </svg>
    </div>
</template>`,

script = `<script type='module'>`;
    import * as Tools from '../js/Tools.js'
    import * as Svg from '../js/Svg.js';
    (function () {
        let
            name='gadget',
            gadget= {
                strokeWidth: 15,
                width: 704,
                height: 1024
            },
            parent = Tools.getSel(`#tpl-${name}`).parentNode.parentNode,
            trigger = parent;
        Svg.svgInit(name, gadget);
        Svg.initAnimate(trigger, 'mouseenter', name, 'anim','anim3','anim4');
        Svg.initAnimate(trigger, 'mouseleave', name, 'anim2','anim5','anim6')
    })();
`</script>`;

//                trigger = parent.querySelector('.caption')
// console.log('jt76t7t')'

//                 let parent = Tools.getSel(#tpl-${name}).parentNode.parentNode,
//    import * as Tools from '../js/Tools.js';
//    Tools.renderTpl('gadget')

