const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');

const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

const webpack = require('webpack');
const merge = require('webpack-merge');

// console.log(process.env)
console.log(process.env.NODE_ENV);
const config = require('./config');
console.log(process.env.NODE_ENV);
if ( !process.env.NODE_ENV ){
    process.env.NODE_ENV = JSON.parse(config.dev.env.NODE_ENV)
}
console.log(process.env.NODE_ENV);

// console.log(process.env.NODE_ENV)
// console.log((config.dev.env.NODE_ENV))
// return

function resolve (dir) {
    // console.log(path.join(__dirname, '.', dir))
    return path.join(__dirname, '.', dir)
}

module.exports = ({
// module.exports = merge(config.dev, {
    mode: process.env.NODE_ENV,
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    output: {
                        comments: false,
                    },
                },
                cache: true,
                parallel: true,
                // extractComments: true,
                // sourceMap: true // set to true if you want JS source maps
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    entry: {
        // My:'./src/js/My.js',
        // Tools:'./src/js/Tools.js',
        // Svg:'./src/js/Svg.js',
        index:'./src/main.js',
    },
    output: {
        filename: 'js/[name].js',
        path: resolve('dist'),
        // library: 'My'
        // library: '[name]'
        // filename: 'index.js',
        // path: resolve('dist')+'/js'
    },
    devtool: (process.env.NODE_ENV==='development')?'inline-source-map':'',
    devServer: {
        // open: true,
        historyApiFallback: true
    },

    module: {
        // loaders: [
        // ],
        rules: [
            // { test: "\.html$", loader: "html-tpl" },
            {
                test: /\.html$/,
                include: resolve('src'),
                use: [ {
                    loader: 'html-loader',
                    options: {
                        minimize: true,
                        collapseWhitespace:true,
                        minifyJS: true
                        // removeComments: true,
                        // collapseWhitespace: true
                    }
                }]
            },
            {
                test: /\.js$/,
                // test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            // minimize: true,
                            // removeComments: true,
                            // collapseWhitespace: true
                        }
                    },
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.pug$/,
                include: resolve('src'),
                // loader: 'pug-loader?+pretty',
                use: [
                    // 'template-html-minifier',
                    {
                        loader:'html-loader',
                        options:{
                            minimize:true,
                            collapseWhitespace:true,
                            minifyJS: true
                        }
                    },
                    // 'html-loader?+minifyJS',
                    // new UglifyJsPlugin(),
                    'pug-html-loader?-pretty',
                    // {
                    //     loader:'html-loader',
                    //     options:{
                    //         minimize:true,
                    //         collapseWhitespace:true,
                    //         minifyJS: true
                    //     }
                    // },

                    // 'url-loader',
                ],
                // loader: ['html-loader?+minimize', 'pug-html-loader?-pretty'], /// !!!
                // loader: ['raw-loader', 'pug-html-loader?-pretty'], //!!!
                // loader: 'pug-loader',
                // options: { pretty: true }
                // options: {
                //     otherHtmlLoaderConfig: {
                //
                //     },
                // }
                // options: {
                // minimize: true,
                // removeComments: true,
                // collapseWhitespace: true
                // }
                // loaders: ['pug-loader'],
                // options: {
                //     pretty: false,
                // }

            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: ('[name].[ext]')
                    // name: ('img/[name].[hash:7].[ext]')
                }
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: ('fonts/[name].[ext]')
                }
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "style.css",
            chunkFilename: "[id].css"
        }),
        new CleanWebpackPlugin(
            ['dist'], { verbose: true, beforeEmit: true, dry: false} // dry: false - затирает
        ),
        new HtmlWebpackPlugin({
            template: './src/tpl/index.pug',
            options: {
                minimize: true,
                removeComments: true,
                collapseWhitespace: true
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new CopyWebpackPlugin([
            { from: 'public' },
            // для того, чтобы обеспечить работу сайта в среде полноценного веб сервера при
            // использовании template
            { from: 'src/js', to: 'js', toType: 'dir' },
        ]),
        new ScriptExtHtmlWebpackPlugin({
                custom:[
                    {
                        test: 'index.js',
                        attribute: 'type',
                        value: 'application/javascript',
                        // value: 'application/javascript;version=1.8',
                        defaultAttribute: 'defer'
                    },
                    {
                        test: /\.js$/,
                        attribute: 'type',
                        value: 'text/javascript',
                        defaultAttribute: 'defer'
                    },
                ]
            }
        ),
        // new webpack.DefinePlugin('NODE_ENV'),
        // new webpack.DefinePlugin({NODE_ENV: 'production'}),
        // new webpack.EnvironmentPlugin({
        //         "process.env.PRODUCTION": JSON.stringify(PRODUCTION),
        //         "proccess.env.DEVELOPMENT": JSON.stringify(DEVELOPMENT),
        // })
        // new webpack.DefinePlugin({NODE_ENV: JSON.stringify(NODE_ENV)})
    ]
});
 