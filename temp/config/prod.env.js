'use strict';

const path = require('path');
const PurgecssPlugin = require('purgecss-webpack-plugin');
const glob = require('glob-all');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
const PurifyCSSPlugin = require('purifycss-webpack');

// const merge = require('webpack-merge')
// const prodEnv = require('./prod.env')
//
// module.exports = merge(prodEnv, {
//     NODE_ENV: '"development"'
// })




module.exports = {
    // NODE_ENV: '"production"',
    plugins:[
        new PurifyCSSPlugin({
            paths: glob.sync(path.join(__dirname, 'dist/*.html')),
        }),
        new PurgecssPlugin({
            paths: glob.sync(path.join(__dirname, 'dist/*.html'))
        }),

    ]

}
