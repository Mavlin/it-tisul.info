import * as Tools from './Tools.js';

let xmlns = "http://www.w3.org/2000/svg",
    getSel = Tools.getSel,
    gid = Tools.gid;

function svgInit(name, arg) {
    let
        parent = getSel(`#tpl-${name}`).parentNode.parentNode,
        svgCanvas = gid(`svg-${name}`),
        trigger = parent,
        // trigger = parent.querySelector('.caption'),
        path = gid(`${name}_svg-path`);
    path.setAttribute('stroke-width', arg.strokeWidth)
    svgCanvas.setAttribute('width', arg.width + arg.strokeWidth)
    svgCanvas.setAttribute('height', arg.height + arg.strokeWidth)
    svgCanvas.setAttribute('viewBox', `${0-arg.strokeWidth/2} ${0-arg.strokeWidth/2}
                ${arg.width + arg.strokeWidth} ${arg.height + arg.strokeWidth}`)

}

function setSvgElem(shape, el) {
    for (let prop in el) {
        if (el.hasOwnProperty(prop)) {
            // console.log(el)
            if (prop==='xlink:href') {
                shape.setAttributeNS('xlink:href', prop, el[prop])
            }else if (prop==='id'){
                shape.setAttributeNS('', 'id',`${el.name}_${el[prop]}`)

            }else{
                shape.setAttributeNS('', prop, el[prop])
            }
        }
    }

}

function renderSvgElem(svg, el) {

    let shape = Tools.gid(`${el.name}_${el.id}`);
    if (!shape) {
        // console.log(`${el.name}${el.id}`)
        // console.log(shape)
        // console.log(svg)
        shape = document.createElementNS(xmlns, el.shape);
        svg.appendChild(shape);
    }
    setSvgElem(shape, el)
}

function initAnimate(el, event, name, ...animations) {
    animations.forEach(function (item) {
        let anim = gid(`${name}_${item}`)
        // console.log(`${name}${item}`)
        // console.log(anim)
        // console.log(anim.tagName)
        if (anim && anim.tagName==='animate') {
            el.addEventListener(event, () => {
                anim.beginElement()
            })
        }
    })
}

function stopAnimate(el, event, name, ...animations) {
    animations.forEach(function (item) {
        // console.log(item)
        let anim = gid(`${name}_${item}`)
        // console.log(`${name}${item}`)
        if (anim && anim.tagName==='animate') {
            el.addEventListener(event, () => {
                anim.endElement()
            })
        }
    })
}
//    function TotalLength(){
//        let path = gid('case');
////        var path = document.querySelector('#case');
//        if (path) {
//            var len = Math.round(path.getTotalLength());
//            console.log("Длина пути - " + len);
//        }
//    };
//    TotalLength()
function log(a) {
    console.log(a)
}

export { log, renderSvgElem, svgInit, initAnimate, stopAnimate }

