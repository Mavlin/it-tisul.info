import "./assets/style.scss";
import * as Tools from './js/Tools.js';
// import * as Svg from './js/Svg.js';

let $ = Tools.getSel,
    $$ = Tools.getSels,
    gid = Tools.gid,
    dialogBoxes = $$('.dialog'),
    content = $('body'),
    overlay = $('.overlay'),
    actions = {
        openPopUp: openPopUp,
        closePopUp: closePopUp,
        sendMessage: sendMessage,
    },
    openPopUps = new Map();

content.addEventListener('click',(e)=>{
    // e.stopPropagation()
    e.preventDefault()
    e.stopPropagation()
    let target, act, dataset, action, el;
    if (e.target) {
        // debugger
        target = e.target// || e.target.closest('[data-action]')
        // dataset = e.target.dataset || e.target.closest('[data-action]')
        if (target){
            el = target.closest('[data-action]')
            if (el) {
                action = el.dataset.action;
                if (action) {
                    act = action.split(':')
                    if (act) {
                        actions[act[0]](act[1] === 'this' ? e : act[1])
                    }
                }
            }
        }
    }
})

function sendMessage(arg) {
    // console.log('sendMessage')
    // console.log(arg)
    // debugger
    let el = arg.target.closest('form'),
        form = new Tools.Form( el ),
        reqData = form.getData(),
        data = new FormData(), i;
    for (i in reqData) {
        if (reqData.hasOwnProperty(i)) data.append(i, reqData[i]);
    }
    let req = {
        'url': './init.php',
        'body': data,
        'method': 'post'
    };
    // console.log(reqData)
    // debugger
    // console.log(form.submitButt)
    new Tools.Fetch(req).run()
        .then(
            response => {
                console.log( response );
                if (response.status === 'ok'){
                    let submitButton = form.submitButt;
                    submitButton.innerHTML = 'доставлено'
                    submitButton.setAttribute('disabled', 'disabled')
                    form.form.addEventListener('input',()=>{
                        submitButton.removeAttribute('disabled')
                        submitButton.innerHTML = 'Отправить<i>&#xe79d;</i>'
                    }, {once:true})
                }
            }
        ).catch(
        error => console.log( error )
    )

}

let foneGrid = {
    row: ()=> {
        return Math.round($('.case-content').scrollHeight/150)
    },
    column: ()=>{
        return Math.round(window.innerWidth/180)
    }
}

window.addEventListener('resize', setFone)

window.addEventListener('load', setFone)

function setFone() {
    let fone = $('.fone'), row, col, i=0, k, ti=0, column, fragment, icon;
    if (fone) {
        fone.innerHTML = ''
        fone.style.height = $('.case-content').scrollHeight + 'px'
        row = foneGrid.row()
        col = foneGrid.column()
        while (i < row) {
            fragment = document.createDocumentFragment();
            column = document.createElement('div')
            column.classList.add('fone-column')
            k = 0
            while (k < col) {
                column.insertAdjacentHTML('beforeEnd', `<i id="fc-icon-${ti++}"></i>`)
                k++
            }
            fragment.appendChild(column)
            fone.appendChild(fragment)
            i++;
        }
        setIcon()
    }
}

let arr = ['&#xe797;','&#xe646;','&#xe649;','&#xe643;',
    '&#xe648;','&#xe791;','&#xe797;','&#xf26c;','&#xf02f;'];

function setIcon() {
    let i=0, k, el, rnd, len, row, col;
    len = arr.length
    row = foneGrid.row()
    col = foneGrid.column()
    while (i<row*col){
        el = gid(`fc-icon-${i++}`)
        if (el){
            rnd = Tools.randomInteger(1, len)-1
            el.innerHTML = arr[rnd]
        }
    }
}


overlay.addEventListener('click',(e)=>{
    closeAllPopUp()
    e.stopPropagation()
})

// Close the dialog - press escape key // key#27
document.addEventListener('keyup', function(e) {
    if (e.keyCode === 27) {
        let cntOpenPopUps = openPopUps.size, i = 0
        openPopUps.forEach((val, key, set)=>{
            if (++i === cntOpenPopUps){
                closePopUp(key)
                set.delete(key)
            }
        })
    }
});

// Close the dialog - click outside
function openPopUp(el) {
    let tpl = gid('tpl-'+el), elId = gid(el);
    if (tpl) {
        elId.innerHTML = tpl.innerHTML
    }
    elId.style.display = 'block'
    setTimeout(() => {
        elId.classList.add('active');
        overlay.classList.add('active')
        openPopUps.set(el, 1); // list of opened popup windows
    }, 20)
    setTimeout(() => {
        let form = elId.querySelector('form'), field;
        if (form) {
            // debugger
            // form.reset()
            field = form.querySelector('[autofocus="autofocus"]')
            if (field) {
                field.focus()
            }
        }
    }, 200)
    // window.scrollBy(0,0)
}


function closePopUp(e) {
    let dialog
    if (e.target){
        dialog = e.target.closest('.dialog')
    } else {
        dialog = gid(e)
    }
    dialog.classList.remove('active');
    openPopUps.delete(dialog.getAttribute('id'))
    // console.log(openPopUps)
    setTimeout(function () {
        dialog.style.display='none'
    },500)
    if (!$$('.dialog.active').length){
        overlay.classList.remove('active')
    }
}

function closeAllPopUp() {
    overlay.classList.remove('active')
    openPopUps.clear();
    dialogBoxes.forEach((item)=>{
        item.classList.remove('active');
        setTimeout(function () {
            item.style.display='none'
        },500)
    })
}

// Tools.renderTpl('gadget')
// import * as Tools from '../js/Tools.js';
Tools.renderTpl('gadget')
// Svg = require('./js/Svg')
// Svg.log(555876786)
