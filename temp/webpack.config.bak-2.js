const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');

// const PurgecssPlugin = require('purgecss-webpack-plugin');
// const glob = require('glob-all');
// // const ExtractTextPlugin = require('extract-text-webpack-plugin');
// const PurifyCSSPlugin = require('purifycss-webpack');

const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

const webpack = require('webpack');
const merge = require('webpack-merge');

// console.log(process.env)
// console.log(process.env.NODE_ENV);
const config = require('./config');
if ( !process.env.NODE_ENV ){
    process.env.NODE_ENV = JSON.parse(config.dev.env.NODE_ENV)
}
console.log(process.env.NODE_ENV);


function resolve (dir) {
    // console.log(path.join(__dirname, '.', dir))
    return path.join(__dirname, '.', dir)
}


// const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
    NODE_ENV: '"development"'
})




module.exports = ({
    mode: process.env.NODE_ENV,
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    output: {
                        comments: false,
                    },
                },
                cache: true,
                parallel: true,
                // extractComments: true,
                // sourceMap: true // set to true if you want JS source maps
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    externals: [
        // 'foundation-sites'
    ],
    entry: {
        // My:'./src/js/My.js',
        // Tools:'./src/js/Tools.js',
        // Svg:'./src/js/Svg.js',
        // "script-loader!jquery/dist/jquery.min.js",
        // "script-loader!foundation-sites/dist/js/foundation.min.js",
        // './src/main.js',
        index: './src/main.js',
        // foundation:"foundation-sites/dist/js/foundation.min.js"
    },
    // "script!foundation-sites/dist/js/foundation.min.js"
    output: {
        filename: 'js/[name].js',
        path: resolve('dist'),
        // library: 'My'
        // library: '[name]'
        // filename: 'index.js',
        // path: resolve('dist')+'/js'
    },
    devtool: (process.env.NODE_ENV==='development')?'inline-source-map':'',
    devServer: {
        // open: true,
        historyApiFallback: true
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.pug$/,
                include: resolve('src'),
                use: [
                    {
                        loader:'html-loader',
                        options:{
                            minimize:true,
                        }
                    },
                    'pug-html-loader?-pretty',
                ],
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: ('[name].[ext]')
                    // name: ('img/[name].[hash:7].[ext]')
                }
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: ('fonts/[name].[ext]')
                }
            }
        ]
    },
    plugins: [
        // new ExtractTextPlugin('[name].[contenthash].css'),
        // new ExtractTextPlugin('index'),
        // new ExtractTextPlugin('dist/style.css'),
        // Make sure this is after ExtractTextPlugin!
        // основной PurgeCss
        // дополнительный PurgeCss, использует устаревшую зависимость,
        // но дополнительно чистит (10%) css
        // new PurifyCSSPlugin({
        //     paths: glob.sync(path.join(__dirname, 'dist/*.html')),
        // }),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        // new PurgecssPlugin({
        //     paths: glob.sync(path.join(__dirname, 'dist/*.html'))
        // }),
        new CleanWebpackPlugin(
            ['dist'], { verbose: true, beforeEmit: true, dry: false} // dry: false - затирает
        ),
        new HtmlWebpackPlugin({
            template: './src/tpl/index.pug',
            options: {
                minimize: true,
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new CopyWebpackPlugin([
            { from: 'public' },
            // для того, чтобы обеспечить работу сайта в среде полноценного веб сервера при
            // использовании template
            { from: 'src/js', to: 'js', toType: 'dir' },
        ]),
        new ScriptExtHtmlWebpackPlugin({
                custom:[
                    {
                        test: 'index.js',
                        attribute: 'type',
                        value: 'application/javascript',
                        // value: 'application/javascript;version=1.8',
                        defaultAttribute: 'defer'
                    },
                    {
                        test: /\.js$/,
                        attribute: 'type',
                        value: 'text/javascript',
                        defaultAttribute: 'defer'
                    },
                ]
            },
        ),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            // foundation: 'script-loader!foundation-sites/dist/js/foundation.min.js'
            // foundation: 'Foundation'
        })
    ],
    resolve: { // чтобы импорты были короче, да и перемещать пакеты легче
        alias: {
            foundation: 'foundation-sites/dist/js'
        },
    }
});
 