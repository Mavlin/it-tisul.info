// const webpack = require('webpack');

module.exports = function () {
    // console.log(process.env.NODE_ENV);
    return {
        devtool: 'inline-source-map',
        devServer: {
            // stats: 'errors-only',
            // port: 9000,
            historyApiFallback: true
        },
        performance: {
            hints: false,
            // maxEntrypointSize: 4000000,
            // maxAssetSize: 4000000
        },
        // plugins:[
        //     new webpack.HotModuleReplacementPlugin()
        // ]
    }
};



