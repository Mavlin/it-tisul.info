const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = function(dir) {
    return {
        plugins: [
            new CleanWebpackPlugin(
            // dry: false - затирает
            //     ['dist'], { verbose: true, beforeEmit: true, dry: false}
                [dir], { verbose: true, root: process.cwd(), beforeEmit: true, dry: false}
            ),
        ]
    };
};